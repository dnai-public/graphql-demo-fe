import React, { ReactElement } from "react";
import { useGetCarMakersQuery } from "data/generated";
import { CustomTable } from "libs/components/table/CustomTable";

export const CarMakers = (): ReactElement => {
	const { data: getCarMakersData } = useGetCarMakersQuery();

	const carMakersData = getCarMakersData?.getCarMakers;

	return (
		<>
			<CustomTable data={carMakersData} />
		</>
	);
};
