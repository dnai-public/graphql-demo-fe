import { Button, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import { GetCarsDocument, useGetCarEnginesQuery, useGetCarModelsQuery, usePostCarMutation } from "data/generated";
import React, { ReactElement } from "react";
import { useForm } from "react-hook-form";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";

type FormValues = {
	vin: string;
	license: string;
	engineCode: string;
	modelCode: string;
	mileage: number;
	year: number;
};

export const AddCar = (): ReactElement => {
	const [postCarMutation] = usePostCarMutation();
	const { data: carModelData } = useGetCarModelsQuery();
	const modelData = carModelData?.getCarModels;
	const { data: carEngineData } = useGetCarEnginesQuery();
	const engineData = carEngineData?.getCarEngines;

	const [modelCode, setModelCode] = React.useState(modelData?.[0].modelNumber);
	const [engineCode, setEngineCode] = React.useState(engineData?.[0].engineCode);

	const handleModeCodeChange = (event: SelectChangeEvent) => {
		setModelCode(event.target.value as string);
	};

	const handleEngineCodeChange = (event: SelectChangeEvent) => {
		setEngineCode(event.target.value as string);
	};

	const { register, handleSubmit } = useForm<FormValues>({
		mode: "onChange",
	});

	const onSubmit = (formData: FormValues): void => {
		postCarMutation({
			variables: {
				input: {
					vin: formData.vin,
					license: formData.license,
					modelCode: modelCode,
					engineCode: engineCode,
					mileage: formData.mileage,
					year: formData.year,
				},
			},
			refetchQueries: [GetCarsDocument],
		});
	};

	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			style={{ marginTop: "3rem", display: "flex", flexDirection: "column", width: "30rem", marginLeft: "auto", marginRight: "auto" }}
		>
			<TextField
				label="Vin"
				variant="outlined"
				autoFocus
				autoComplete="off"
				type="text"
				size="small"
				placeholder="Vin"
				{...register("vin", {
					required: "Vin is required",
				})}
				id="vin"
				sx={{ marginBottom: "2rem" }}
			/>
			<TextField
				label="License"
				variant="outlined"
				autoFocus
				autoComplete="off"
				type="text"
				size="small"
				placeholder="License"
				{...register("license", {
					required: "License is required",
				})}
				id="license"
				sx={{ marginBottom: "2rem" }}
			/>

			<InputLabel id="modelCode">Model Code</InputLabel>
			<Select
				id="modelCode"
				value={modelCode}
				size="small"
				label="Model Code"
				onChange={handleModeCodeChange}
				sx={{ marginBottom: "2rem" }}
			>
				{modelData?.map((item) => (
					<MenuItem value={item.modelNumber} key={item.modelNumber}>
						{item.modelNumber}
					</MenuItem>
				))}
			</Select>

			<InputLabel id="engineCode">Engine Code</InputLabel>
			<Select
				id="engineCode"
				value={engineCode}
				size="small"
				label="Engine Code"
				onChange={handleEngineCodeChange}
				sx={{ marginBottom: "2rem" }}
			>
				{engineData?.map((item) => (
					<MenuItem value={item.engineCode} key={item.engineCode}>
						{item.engineCode}
					</MenuItem>
				))}
			</Select>

			<TextField
				label="Mileage"
				variant="outlined"
				autoFocus
				autoComplete="off"
				type="number"
				size="small"
				placeholder="Mileage"
				{...register("mileage", {
					required: "Mileage is required",
				})}
				id="mileage"
				sx={{ marginBottom: "2rem" }}
			/>
			<TextField
				label="Year"
				variant="outlined"
				autoFocus
				autoComplete="off"
				type="number"
				size="small"
				placeholder="Year"
				{...register("year", {
					required: "Year is required",
				})}
				id="year"
				sx={{ marginBottom: "2rem" }}
			/>
			<Button variant="contained" type="submit" startIcon={<AddCircleOutlineIcon />}>
				Create Car
			</Button>
		</form>
	);
};
