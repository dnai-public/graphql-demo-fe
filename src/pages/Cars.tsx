import React, { ReactElement, useState } from "react";
import { Box, Table } from "@mui/material";
import { TableRow } from "@mui/material";
import { TableCell } from "@mui/material";
import { TableHead } from "@mui/material";
import { TableContainer } from "@mui/material";
import Paper from "@mui/material/Paper";
import { TableBody } from "@mui/material";
import { useGetCarsQuery } from "data/generated";
import Button from "@mui/material/Button";
import { useShowDialog } from "libs/components/dialog/useShowDialog";

export const Cars = (): ReactElement => {
	const { data: getCarsData, loading: getCarsLoading } = useGetCarsQuery();
	const { showDialog, DialogCarModels } = useShowDialog();
	const [modelNumber, setModelNumber] = useState<string>("");

	const carsData = getCarsData?.getCars;

	if (getCarsLoading || !carsData) {
		return <>Loading</>;
	}

	const handleClick = (modelNumber: string) => {
		setModelNumber(modelNumber);
		showDialog();
	};

	const tableHeadData = Object.keys(carsData[0]).map((item) => item.replace("__typename", ""));

	return (
		<>
			<Box sx={{ display: "flex", alignItems: "center", justifyContent: "center", marginTop: "2rem" }}>
				<TableContainer component={Paper} sx={{ width: "75%" }}>
					<Table aria-label="simple table">
						<TableHead>
							<TableRow>
								{tableHeadData.map((tableHeadName) => (
									<TableCell key={tableHeadName}>{tableHeadName}</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{carsData.map((row, index) => (
								<TableRow key={row.license}>
									{Object.values(row).map((cell) =>
										typeof cell === "string" || typeof cell === "number" ? (
											<TableCell align="left" key={cell}>
												{cell}
											</TableCell>
										) : (
											<TableCell align="left" key={cell.__typename}>
												<Button variant="contained" onClick={() => handleClick(cell.modelNumber)}>
													{cell.name}
												</Button>
											</TableCell>
										)
									)}
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Box>

			<DialogCarModels parameter={modelNumber} />
		</>
	);
};
