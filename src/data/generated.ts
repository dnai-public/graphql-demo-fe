import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
import * as ApolloReactHooks from "libs/utils/graphqlHooks";
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
	ID: string;
	String: string;
	Boolean: boolean;
	Int: number;
	Float: number;
};

export type AbstractCarEngine = {
	engineCode: Scalars["String"];
	models: Array<CarModel>;
	power: Scalars["Int"];
	torque: Scalars["Int"];
};

export type AbstractCarEnginePowerArgs = {
	unit?: EPowerUnit;
};

export type Car = {
	__typename?: "Car";
	engine: AbstractCarEngine;
	license: Scalars["String"];
	manufactureYear: Scalars["Int"];
	mileage: Scalars["Int"];
	model: CarModel;
	vin: Scalars["String"];
};

export type CarEngineElectric = AbstractCarEngine & {
	__typename?: "CarEngineElectric";
	engineCode: Scalars["String"];
	engineType: ElectricEngineType;
	models: Array<CarModel>;
	power: Scalars["Int"];
	torque: Scalars["Int"];
	voltage: Scalars["Int"];
};

export type CarEngineElectricPowerArgs = {
	unit?: EPowerUnit;
};

export type CarEngineElectricInput = {
	code?: InputMaybe<Scalars["String"]>;
	engineType?: InputMaybe<EElectricEngineType>;
	modelIds: Array<Scalars["String"]>;
	power?: InputMaybe<Scalars["Int"]>;
	torque?: InputMaybe<Scalars["Int"]>;
	voltage?: InputMaybe<Scalars["Int"]>;
};

export type CarEngineIC = AbstractCarEngine & {
	__typename?: "CarEngineIC";
	cylinderCount: Scalars["Int"];
	displacement: Scalars["Int"];
	engineCode: Scalars["String"];
	fuelType?: Maybe<EFuelType>;
	models: Array<CarModel>;
	power: Scalars["Int"];
	torque: Scalars["Int"];
};

export type CarEngineICPowerArgs = {
	unit?: EPowerUnit;
};

export type CarEngineICInput = {
	code?: InputMaybe<Scalars["String"]>;
	cylinderCount?: InputMaybe<Scalars["Int"]>;
	displacement?: InputMaybe<Scalars["Int"]>;
	fuelType?: InputMaybe<EFuelType>;
	modelIds: Array<Scalars["String"]>;
	power?: InputMaybe<Scalars["Int"]>;
	torque?: InputMaybe<Scalars["Int"]>;
};

export type CarInput = {
	engineCode?: InputMaybe<Scalars["String"]>;
	license?: InputMaybe<Scalars["String"]>;
	mileage?: InputMaybe<Scalars["Int"]>;
	modelCode?: InputMaybe<Scalars["String"]>;
	vin?: InputMaybe<Scalars["String"]>;
	year?: InputMaybe<Scalars["Int"]>;
};

export type CarMaker = {
	__typename?: "CarMaker";
	country: Scalars["String"];
	established: Scalars["Int"];
	models: Array<CarModel>;
	name: Scalars["String"];
};

export type CarMakerInput = {
	country?: InputMaybe<Scalars["String"]>;
	established?: InputMaybe<Scalars["Int"]>;
	name?: InputMaybe<Scalars["String"]>;
};

export type CarModel = {
	__typename?: "CarModel";
	availableEngines: Array<AbstractCarEngine>;
	body: Scalars["String"];
	manufacturer: CarMaker;
	modelNumber: Scalars["String"];
	name: Scalars["String"];
};

export type CarModelAvailableEnginesArgs = {
	filter?: InputMaybe<EngineFilter>;
};

export type CarModelInput = {
	body?: InputMaybe<Scalars["String"]>;
	engineCodes: Array<Scalars["String"]>;
	manufacturerId?: InputMaybe<Scalars["String"]>;
	modelNumber?: InputMaybe<Scalars["String"]>;
	name?: InputMaybe<Scalars["String"]>;
};

export enum EElectricEngineType {
	AC = "AC",
	BLDC = "BLDC",
	DC = "DC",
	PMSM = "PMSM",
}

export enum EFuelType {
	DIESEL = "DIESEL",
	GASOLINE = "GASOLINE",
}

export enum EPowerUnit {
	HP = "HP",
	KW = "KW",
}

export type ElectricEngineType = {
	__typename?: "ElectricEngineType";
	code: Scalars["String"];
	name: Scalars["String"];
};

export type EngineFilter = {
	models?: Array<Scalars["String"]>;
	powerMax?: InputMaybe<Scalars["Int"]>;
	powerMin?: InputMaybe<Scalars["Int"]>;
	torqueMax?: InputMaybe<Scalars["Int"]>;
	torqueMin?: InputMaybe<Scalars["Int"]>;
};

export type Mutation = {
	__typename?: "Mutation";
	postCar?: Maybe<Car>;
	postCarEngineElectric?: Maybe<CarEngineElectric>;
	postCarEngineIC?: Maybe<CarEngineIC>;
	postCarMaker?: Maybe<CarMaker>;
	postCarModel?: Maybe<CarModel>;
};

export type MutationPostCarArgs = {
	input: CarInput;
};

export type MutationPostCarEngineElectricArgs = {
	input?: InputMaybe<CarEngineElectricInput>;
};

export type MutationPostCarEngineICArgs = {
	input: CarEngineICInput;
};

export type MutationPostCarMakerArgs = {
	input: CarMakerInput;
};

export type MutationPostCarModelArgs = {
	input: CarModelInput;
};

export type Query = {
	__typename?: "Query";
	getCar?: Maybe<Car>;
	getCarByLic?: Maybe<Car>;
	getCarByVin?: Maybe<Car>;
	getCarEngine?: Maybe<AbstractCarEngine>;
	getCarEngines: Array<AbstractCarEngine>;
	getCarMaker?: Maybe<CarMaker>;
	getCarMakers: Array<CarMaker>;
	getCarModel?: Maybe<CarModel>;
	getCarModels: Array<CarModel>;
	getCars: Array<Car>;
};

export type QueryGetCarArgs = {
	license?: Scalars["String"];
	vin?: Scalars["String"];
};

export type QueryGetCarByLicArgs = {
	license: Scalars["String"];
};

export type QueryGetCarByVinArgs = {
	vin: Scalars["String"];
};

export type QueryGetCarEngineArgs = {
	code: Scalars["String"];
};

export type QueryGetCarEnginesArgs = {
	filter?: InputMaybe<EngineFilter>;
};

export type QueryGetCarMakerArgs = {
	name: Scalars["String"];
};

export type QueryGetCarModelArgs = {
	modelNumber: Scalars["String"];
};

export type QueryGetCarModelsArgs = {
	modelNums?: Array<Scalars["String"]>;
};

export type GetCarEnginesQueryVariables = Exact<{ [key: string]: never }>;

export type GetCarEnginesQuery = {
	__typename?: "Query";
	getCarEngines: Array<
		| { __typename?: "CarEngineElectric"; engineCode: string; power: number; torque: number }
		| { __typename?: "CarEngineIC"; engineCode: string; power: number; torque: number }
	>;
};

export type GetCarMakersQueryVariables = Exact<{ [key: string]: never }>;

export type GetCarMakersQuery = {
	__typename?: "Query";
	getCarMakers: Array<{
		__typename?: "CarMaker";
		name: string;
		country: string;
		established: number;
		models: Array<{ __typename?: "CarModel"; modelNumber: string; name: string; body: string }>;
	}>;
};

export type GetCarModelQueryVariables = Exact<{
	modelNumber: Scalars["String"];
}>;

export type GetCarModelQuery = {
	__typename?: "Query";
	getCarModel?: {
		__typename?: "CarModel";
		modelNumber: string;
		name: string;
		body: string;
		manufacturer: { __typename?: "CarMaker"; name: string; country: string; established: number };
	} | null;
};

export type GetCarModelsQueryVariables = Exact<{ [key: string]: never }>;

export type GetCarModelsQuery = {
	__typename?: "Query";
	getCarModels: Array<{
		__typename?: "CarModel";
		modelNumber: string;
		name: string;
		body: string;
		manufacturer: { __typename?: "CarMaker"; name: string; country: string; established: number };
	}>;
};

export type GetCarsQueryVariables = Exact<{ [key: string]: never }>;

export type GetCarsQuery = {
	__typename?: "Query";
	getCars: Array<{
		__typename?: "Car";
		vin: string;
		license: string;
		mileage: number;
		manufactureYear: number;
		model: { __typename?: "CarModel"; modelNumber: string; name: string; body: string };
	}>;
};

export type PostCarMutationVariables = Exact<{
	input: CarInput;
}>;

export type PostCarMutation = {
	__typename?: "Mutation";
	postCar?: {
		__typename?: "Car";
		vin: string;
		license: string;
		mileage: number;
		manufactureYear: number;
		model: { __typename?: "CarModel"; modelNumber: string; name: string; body: string };
	} | null;
};

export const GetCarEnginesDocument = gql`
	query getCarEngines {
		getCarEngines(filter: null) {
			engineCode
			power
			torque
		}
	}
`;

/**
 * __useGetCarEnginesQuery__
 *
 * To run a query within a React component, call `useGetCarEnginesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCarEnginesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCarEnginesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCarEnginesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCarEnginesQuery, GetCarEnginesQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useQuery<GetCarEnginesQuery, GetCarEnginesQueryVariables>(GetCarEnginesDocument, options);
}
export function useGetCarEnginesLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCarEnginesQuery, GetCarEnginesQueryVariables>
) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useLazyQuery<GetCarEnginesQuery, GetCarEnginesQueryVariables>(GetCarEnginesDocument, options);
}
export type GetCarEnginesQueryHookResult = ReturnType<typeof useGetCarEnginesQuery>;
export type GetCarEnginesLazyQueryHookResult = ReturnType<typeof useGetCarEnginesLazyQuery>;
export type GetCarEnginesQueryResult = Apollo.QueryResult<GetCarEnginesQuery, GetCarEnginesQueryVariables>;
export const GetCarMakersDocument = gql`
	query getCarMakers {
		getCarMakers {
			name
			country
			established
			models {
				modelNumber
				name
				body
			}
		}
	}
`;

/**
 * __useGetCarMakersQuery__
 *
 * To run a query within a React component, call `useGetCarMakersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCarMakersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCarMakersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCarMakersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCarMakersQuery, GetCarMakersQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useQuery<GetCarMakersQuery, GetCarMakersQueryVariables>(GetCarMakersDocument, options);
}
export function useGetCarMakersLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCarMakersQuery, GetCarMakersQueryVariables>
) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useLazyQuery<GetCarMakersQuery, GetCarMakersQueryVariables>(GetCarMakersDocument, options);
}
export type GetCarMakersQueryHookResult = ReturnType<typeof useGetCarMakersQuery>;
export type GetCarMakersLazyQueryHookResult = ReturnType<typeof useGetCarMakersLazyQuery>;
export type GetCarMakersQueryResult = Apollo.QueryResult<GetCarMakersQuery, GetCarMakersQueryVariables>;
export const GetCarModelDocument = gql`
	query getCarModel($modelNumber: String!) {
		getCarModel(modelNumber: $modelNumber) {
			modelNumber
			name
			body
			manufacturer {
				name
				country
				established
			}
		}
	}
`;

/**
 * __useGetCarModelQuery__
 *
 * To run a query within a React component, call `useGetCarModelQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCarModelQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCarModelQuery({
 *   variables: {
 *      modelNumber: // value for 'modelNumber'
 *   },
 * });
 */
export function useGetCarModelQuery(baseOptions: ApolloReactHooks.QueryHookOptions<GetCarModelQuery, GetCarModelQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useQuery<GetCarModelQuery, GetCarModelQueryVariables>(GetCarModelDocument, options);
}
export function useGetCarModelLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCarModelQuery, GetCarModelQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useLazyQuery<GetCarModelQuery, GetCarModelQueryVariables>(GetCarModelDocument, options);
}
export type GetCarModelQueryHookResult = ReturnType<typeof useGetCarModelQuery>;
export type GetCarModelLazyQueryHookResult = ReturnType<typeof useGetCarModelLazyQuery>;
export type GetCarModelQueryResult = Apollo.QueryResult<GetCarModelQuery, GetCarModelQueryVariables>;
export const GetCarModelsDocument = gql`
	query getCarModels {
		getCarModels(modelNums: []) {
			modelNumber
			name
			body
			manufacturer {
				name
				country
				established
			}
		}
	}
`;

/**
 * __useGetCarModelsQuery__
 *
 * To run a query within a React component, call `useGetCarModelsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCarModelsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCarModelsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCarModelsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCarModelsQuery, GetCarModelsQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useQuery<GetCarModelsQuery, GetCarModelsQueryVariables>(GetCarModelsDocument, options);
}
export function useGetCarModelsLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCarModelsQuery, GetCarModelsQueryVariables>
) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useLazyQuery<GetCarModelsQuery, GetCarModelsQueryVariables>(GetCarModelsDocument, options);
}
export type GetCarModelsQueryHookResult = ReturnType<typeof useGetCarModelsQuery>;
export type GetCarModelsLazyQueryHookResult = ReturnType<typeof useGetCarModelsLazyQuery>;
export type GetCarModelsQueryResult = Apollo.QueryResult<GetCarModelsQuery, GetCarModelsQueryVariables>;
export const GetCarsDocument = gql`
	query getCars {
		getCars {
			vin
			license
			mileage
			manufactureYear
			model {
				modelNumber
				name
				body
			}
		}
	}
`;

/**
 * __useGetCarsQuery__
 *
 * To run a query within a React component, call `useGetCarsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCarsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCarsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCarsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCarsQuery, GetCarsQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useQuery<GetCarsQuery, GetCarsQueryVariables>(GetCarsDocument, options);
}
export function useGetCarsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCarsQuery, GetCarsQueryVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useLazyQuery<GetCarsQuery, GetCarsQueryVariables>(GetCarsDocument, options);
}
export type GetCarsQueryHookResult = ReturnType<typeof useGetCarsQuery>;
export type GetCarsLazyQueryHookResult = ReturnType<typeof useGetCarsLazyQuery>;
export type GetCarsQueryResult = Apollo.QueryResult<GetCarsQuery, GetCarsQueryVariables>;
export const PostCarDocument = gql`
	mutation postCar($input: CarInput!) {
		postCar(input: $input) {
			vin
			license
			mileage
			manufactureYear
			model {
				modelNumber
				name
				body
			}
		}
	}
`;
export type PostCarMutationFn = Apollo.MutationFunction<PostCarMutation, PostCarMutationVariables>;

/**
 * __usePostCarMutation__
 *
 * To run a mutation, you first call `usePostCarMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePostCarMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [postCarMutation, { data, loading, error }] = usePostCarMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePostCarMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<PostCarMutation, PostCarMutationVariables>) {
	const options = { ...defaultOptions, ...baseOptions };
	return ApolloReactHooks.useMutation<PostCarMutation, PostCarMutationVariables>(PostCarDocument, options);
}
export type PostCarMutationHookResult = ReturnType<typeof usePostCarMutation>;
export type PostCarMutationResult = Apollo.MutationResult<PostCarMutation>;
export type PostCarMutationOptions = Apollo.BaseMutationOptions<PostCarMutation, PostCarMutationVariables>;
