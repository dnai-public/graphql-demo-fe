import { ApolloError } from "@apollo/client";
import { QueryResult } from "@apollo/client/react/types/types";
import { QueryResultStatus } from "./QueryResultStatus";

export const getQueryResultStatus = (queryResult: QueryResult<any, any>): QueryResultStatus => {
	const loaded: boolean = queryResult.called && !queryResult.loading && !queryResult.error;
	const loading: boolean = queryResult.loading;
	const loadingInitial: boolean = !queryResult.data && !queryResult.previousData && !queryResult.error && queryResult.loading;
	const errors: ApolloError[] = queryResult.error ? [queryResult.error!] : [];
	const error: boolean = errors.length > 0;

	return { loaded, loading, loadingInitial, errors, error };
};

export const mergeQueryStatus = (...statusList: QueryResultStatus[]): QueryResultStatus => {
	const loaded: boolean = statusList.every((status) => status.loaded);
	const loading: boolean = statusList.some((status) => status.loading);
	const loadingInitial: boolean = statusList.some((status) => status.loadingInitial);
	const errors: ApolloError[] = statusList.flatMap((status) => status.errors);
	const error: boolean = errors.length > 0;

	return { loaded, loading, loadingInitial, errors, error };
};
