import Apollo from "@apollo/client/react/types/types";
import { TypedDocumentNode } from "@apollo/client/core/types";
import { DefinitionNode, DocumentNode, OperationDefinitionNode } from "graphql";
import { getQueryResultStatus } from "./mergeQueryStatus";
import { useQuery as useApolloQuery } from "@apollo/client";
import { QueryResultStatus } from "./QueryResultStatus";
export { useMutation } from "@apollo/client";
export type { MutationHookOptions } from "@apollo/client";

export type QueryResult<TData> = Apollo.QueryResult<TData> & {
	result?: TData;
	status: QueryResultStatus;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const parseQuery = <TData, TVariables>(
	apolloResult: Apollo.QueryResult,
	dataPropertyName: string,
	defaultValue?: TData
): QueryResult<TData> => {
	const result: TData = apolloResult.data?.[dataPropertyName] || defaultValue;
	const status = getQueryResultStatus(apolloResult);
	return { ...apolloResult, result, status };
};

const isOperationDefinition = (item: DefinitionNode): item is OperationDefinitionNode => {
	return item.kind === "OperationDefinition";
};

const getOperationName = (query: DocumentNode): string => {
	const operationDefinition: OperationDefinitionNode | undefined = query.definitions.find(isOperationDefinition);
	if (operationDefinition?.name) {
		return operationDefinition.name.value;
	}
	throw new Error("GraphQLUtils.useQuery - OperationDefinitionNode or name of OperationDefinitionNode is missing.");
};

export type QueryHookOptions<TData, TVariables> = Apollo.QueryHookOptions<TData, TVariables> & {
	defaults?: TData;
};

export const useQuery = <TData, TVariables>(
	query: DocumentNode | TypedDocumentNode<TData, TVariables>,
	options?: QueryHookOptions<TData, TVariables>
): QueryResult<TData> => {
	const dataPropertyName: string = getOperationName(query);
	const obj: any = useApolloQuery<TData, TVariables>(query, options);

	return parseQuery<TData, TVariables>(obj, dataPropertyName, options?.defaults);
};

export type LazyQueryHookOptions<TData, TVariables> = Apollo.LazyQueryHookOptions<TData, TVariables> & {
	defaults?: TData;
};

export const useLazyQuery = <TData, TVariables>(
	query: DocumentNode | TypedDocumentNode<TData, TVariables>,
	options?: LazyQueryHookOptions<TData, TVariables>
): QueryResult<TData> => {
	const dataPropertyName: string = getOperationName(query);
	const obj: any = useApolloQuery<TData, TVariables>(query, options);

	return parseQuery<TData, TVariables>(obj, dataPropertyName, options?.defaults);
};
