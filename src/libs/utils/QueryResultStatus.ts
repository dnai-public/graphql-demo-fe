import { ApolloError } from "@apollo/client";

export type QueryResultStatus = {
	loaded: boolean;
	loading: boolean; // true if loading
	loadingInitial: boolean; // true only during the first load
	error: boolean;
	errors: ApolloError[];
};
