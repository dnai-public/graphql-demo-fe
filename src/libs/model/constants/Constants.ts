const windowEnvs: Record<string, string | undefined> = (globalThis.window as any).__envs__ || {};

export class Constants {
	static readonly GRAPHQL_ENDPOINT = (windowEnvs.GRAPHQL_ENDPOINT || process.env.GRAPHQL_ENDPOINT)!;
}
