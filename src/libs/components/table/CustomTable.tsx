import { Box, Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useShowDialog } from "libs/components/dialog/useShowDialog";
import React, { ReactElement, useState } from "react";

type Props = {
	data?: any;
};

export const CustomTable = (props: Props): ReactElement => {
	const { data } = props;
	const { showDialog, DialogCarModels } = useShowDialog();
	const [modelNumber, setModelNumber] = useState<string>("");

	if (!data) {
		return <>Loading</>;
	}

	const handleClick = (modelNumber: string) => {
		setModelNumber(modelNumber);
		showDialog();
	};

	const tableHeadData = Object.keys(data[0]).map((item) => item.replace("__typename", ""));

	return (
		<>
			<Box sx={{ display: "flex", alignItems: "center", justifyContent: "center", marginTop: "2rem" }}>
				<TableContainer component={Paper} sx={{ width: "75%" }}>
					<Table aria-label="simple table">
						<TableHead>
							<TableRow>
								{tableHeadData.map((tableHeadName) => (
									<TableCell key={tableHeadName}>{tableHeadName}</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{data.map((row: any, index: any) => (
								<TableRow key={index}>
									{Object.values(row).map((cell: any) =>
										typeof cell === "string" || typeof cell === "number" ? (
											<TableCell align="left" key={cell}>
												{cell}
											</TableCell>
										) : (
											<TableCell align="left" key={index}>
												{cell?.map((item: any) => (
													<Button
														key={item.name}
														variant="contained"
														onClick={() => handleClick(item.modelNumber)}
														sx={{ marginRight: "1rem" }}
													>
														{item.name}
													</Button>
												))}
											</TableCell>
										)
									)}
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Box>
			<DialogCarModels parameter={modelNumber} />
		</>
	);
};
