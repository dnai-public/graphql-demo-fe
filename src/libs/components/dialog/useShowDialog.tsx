import React from "react";
import { ReactElement, ReactNode } from "react";
import { Button, Dialog, DialogContent, DialogTitle, List, ListItem } from "@mui/material";
import { useGetCarModelQuery } from "data/generated";
import { useNavigate } from "react-router-dom";

type ReturnType = {
	showDialog: () => void;
	DialogCarModels: (props: Props) => ReactElement;
};

type Props = {
	title?: ReactNode;
	children?: ReactNode | undefined;
	parameter: string;
};

export const useShowDialog = (): ReturnType => {
	const [open, setOpen] = React.useState<boolean>(false);
	const navigate = useNavigate();
	const onClose = () => {
		setOpen(false);
	};

	const showDialog = () => {
		setOpen(true);
	};

	const DialogCarModels = (props: Props): ReactElement => {
		const { data } = useGetCarModelQuery({
			variables: {
				modelNumber: props.parameter,
			},
		});
		const carModelData = data?.getCarModel;

		return (
			<>
				<Dialog open={open} onClose={onClose}>
					{props.title && <DialogTitle>{props.title}</DialogTitle>}
					<DialogContent>
						<List>
							{carModelData &&
								Object.values(carModelData).map((item) =>
									typeof item === "string" || typeof item === "number" ? (
										<ListItem key={item}>{item}</ListItem>
									) : (
										<Button
											variant="contained"
											onClick={() => {
												navigate("/makers");
												setOpen(false);
											}}
											key={item.name}
										>
											{item.name}
										</Button>
									)
								)}
						</List>
					</DialogContent>
				</Dialog>
			</>
		);
	};

	return { showDialog, DialogCarModels };
};
