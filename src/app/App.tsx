import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import React, { ReactElement } from "react";
import { BrowserRouter } from "react-router-dom";
import { AppHeader } from "./view/AppHeader";
import { AppRouter } from "./view/AppRouter";

export const App = (): ReactElement => {
	const apolloClient = new ApolloClient({
		uri: "http://localhost:8080/graphql",
		cache: new InMemoryCache(),
	});

	return (
		<React.StrictMode>
			<BrowserRouter>
				<ApolloProvider client={apolloClient}>
					<AppHeader />
					<AppRouter />
				</ApolloProvider>
			</BrowserRouter>
		</React.StrictMode>
	);
};
