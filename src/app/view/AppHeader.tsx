import { Toolbar } from "@mui/material";
import { Button } from "@mui/material";
import { AppBar } from "@mui/material";
import React, { ReactElement } from "react";
import DriveEtaIcon from "@mui/icons-material/DriveEta";
import SettingsIcon from "@mui/icons-material/Settings";
import BuildIcon from "@mui/icons-material/Build";
import { useNavigate } from "react-router-dom";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";

export const AppHeader = (): ReactElement => {
	const navigate = useNavigate();
	return (
		<AppBar position="static">
			<Toolbar sx={{ display: "flex", gap: "5rem" }}>
				<Button color="inherit" startIcon={<DriveEtaIcon />} onClick={() => navigate("/")}>
					Cars
				</Button>

				<Button color="inherit" startIcon={<BuildIcon />} onClick={() => navigate("/makers")}>
					Car Makers
				</Button>

				<Button color="inherit" startIcon={<SettingsIcon />} onClick={() => navigate("/engines")}>
					Car Engines
				</Button>
				<div style={{ flex: "1 1 auto" }}></div>
				<Button color="inherit" startIcon={<AddCircleOutlineIcon />} onClick={() => navigate("/addCar")}>
					Add Car
				</Button>
			</Toolbar>
		</AppBar>
	);
};
