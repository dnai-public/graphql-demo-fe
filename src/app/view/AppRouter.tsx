import { AddCar } from "pages/AddCar";
import { CarEngines } from "pages/CarEngines";
import { CarMakers } from "pages/CarMakers";
import { Cars } from "pages/Cars";
import { NotFoundPage } from "pages/NotFoundPage";
import React, { ReactElement } from "react";
import { Route, Routes } from "react-router-dom";

export const AppRouter = (): ReactElement => {
	return (
		<Routes>
			<Route path="/" element={<Cars />} />
			<Route path="/engines" element={<CarEngines />} />
			<Route path="/makers" element={<CarMakers />} />
			<Route path="/addCar" element={<AddCar />} />
			<Route path="*" element={<NotFoundPage />} />
		</Routes>
	);
};
